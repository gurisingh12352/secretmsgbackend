const express = require("express");
const cors = require("cors");
const client = require("./databasepg");
const CookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const passport = require("passport");
//---hashing modules
const bcrypt = require("bcrypt");
const saltRound = 10;

const initializePassport = require("./passport");


const { Cookie } = require("express-session");
const flash = require("express-flash");
const { json } = require("body-parser");

//------------------
const app = express();
const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));
var userdata; //this varible is used for storing data of users which is fetched from data_base

//middleware
app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  })
);
app.use(express.json());
app.use(
  session({
    secret: process.env.SEASSION_SECRETKEY,
    resave: true,
    saveUninitialized:true,
  })
);
app.use(CookieParser(process.env.SEASSION_SECRETKEY));
//intializing passport and session
app.use(passport.initialize());
app.use(passport.session());
initializePassport(passport);
//Registration post req handling
app.post("/register", async function (req, res) {
  // console.log(req.body.name);
  // res.send("server post response");
  //registration data
  const name = req.body.name;
  const email = req.body.email;
  const number = req.body.number;
  const password = req.body.password;

  try {
    //fetching email from database and storing in this variable
    const Email_from_db = await client.query("SELECT * FROM users WHERE email =$1", [email]);
    //if dosn't exist in database perform this
    if (Email_from_db.rows <= 0 && Email_from_db.rows.length <= 0) {
      //if user do not exist will allow them to register from new email
      //hashing password and call back return two err and hashed value
      bcrypt.hash(password, parseInt(saltRound), async function (err, encrypted) {
        if (!err) {
          await client.query("INSERT INTO USERS( name,email,number,password) VALUES($1,$2,$3,$4)", [name, email, number, encrypted]);
          console.log("registration succesfull");
          const userobj={
            name:name,
            email:email,
            number:number
          }
          res.send(userobj)
        }
      });
    }

    //else we will tell that this email already in use
    else {
      console.log("this user already exist");
    }
  } catch {
    console.log("this is critic error");
  }
});

//login post req handling--------------------------------------------------------------
app.post("/login", (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) throw err;
    if (!user) res.send("no user exist with this email");
    else{
      req.logIn(user,err => {
        if (err) throw err;
        res.send("user authanicate successfully");
        console.log(req.isAuthenticated());
      });
    }
  })(req, res, next);
});
//sending response base on if user is loged in or not


app.get("/", (req, res) => {
  if (req.user) {
    res.status(200).json(req.user);
  } else {
    res.status(400).json("No user found");
  }
});
//if user is authanticated or not 
const loged={
  loged:true
}
const Notloged={
   Notloged:false
}
function checkUserIsauthanticated(req,res,next){
  if(req.isAuthenticated())
  {
    return res.send(isAuthenticated);
  }
  next()
}
function checkUserIsNotauthanticated(req,res,next){
  if(req.isAuthenticated()){
    next()
  }
  return res.send('not auth ')
}


//server listening port
app.listen(port, () => {
  console.log(`server is up on port :${port}`);
});
